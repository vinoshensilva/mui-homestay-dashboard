// Import libraries
import { 
    createServer,
    Factory,
    Model,
    Response
} from 'miragejs';

// Import functions
import { asyncGetBase64 } from './functions/file';
import { formDataToJSON } from './functions/formdata';

export const startServer = ()=>{
    createServer({
        models: {
            house: Model
        },
        seeds(server){
            server.createList("house",9);
        },
        factories: {
            house: Factory.extend({
                name(i){
                    return `House ${i+1}`
                },
                image(){
                    return '/images/house.webp'
                },
                location(i){
                    return `Location ${i+1}`
                }
            })
        },
        routes(){
            this.namespace = "api";
            this.logging = false;

            this.get("/houses")
            this.get("/houses/:id")
            
            this.post("/houses",async (schema,request)=>{
                const uploadFile = request.requestBody.get('file');

                const json = formDataToJSON(request.requestBody);
                
                const {
                    name,
                    location
                } = json;

                const base64 = await asyncGetBase64(uploadFile);

                const attrs = {
                    name,
                    location,
                    image: base64
                };

                return schema.houses.create(attrs)
            })
            
            this.patch("/houses/:id",async (schema, request) => {
                let id = request.params.id
                let house = schema.houses.find(id)

                const json = formDataToJSON(request.requestBody);
                
                const {
                    name,
                    location
                } = json;

                const newAttrs = {
                    name,
                    location
                };

                const uploadFile = request.requestBody.get('file');

                if(uploadFile){
                    const base64 = await asyncGetBase64(uploadFile);

                    newAttrs.image = base64;
                }

                return house.update(newAttrs)
            })

            this.delete("/houses/:id",(schema,request)=>{
                let id = request.params.id

                return schema.houses.find(id).destroy()
            })
        }
    })
} 