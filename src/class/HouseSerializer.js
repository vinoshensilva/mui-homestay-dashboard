
class HouseSerializer {
    static Deserialize(data){
        return {
            name: data.name,
            location: data.location,
            image: data.image,
            id: data.id
        }
    }

    // static Serialize(name,location,image){
    //     return {
    //         name: name,
    //         location: location,
    //         image: image,
    //     }
    // }
}

export default HouseSerializer;