

export const jsonToFormData = (json = {})=>{
    const formData = new FormData();

    const keys = Object.keys(json);

    for(let index in keys){
        const key = keys[index];

        if(!([null,undefined].includes(json[key])))
        {
            formData.append(key,json[key]);
        }
    }

    return formData;
}

export const formDataToJSON = (formData)=>{
    const object = {};

    formData.forEach(function(value,key){
        object[key] = value;
    });

    return object;
}