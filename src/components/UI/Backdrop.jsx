// Import material ui components
import {default as MUIBackdrop} from "@mui/material/Backdrop";
import CircularProgress from "@mui/material/CircularProgress";

const LoadingBackdrop = () => {
  return (
    <MUIBackdrop
      sx={{ zIndex: (theme) => theme.zIndex.drawer + 1 }}
      open={true}
    >
      <CircularProgress color="primary" />
    </MUIBackdrop>
  );
};

export default LoadingBackdrop;
