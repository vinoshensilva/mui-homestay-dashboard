// Import libraries


// Import library
import { Link } from 'react-router-dom';

// Import material ui components
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";

const Navbar = () => {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <Link style={{textDecoration: 'none',color: "inherit"}} to="/">
            <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
              HomeStay
            </Typography>
          </Link>
        </Toolbar>
      </AppBar>
    </Box>
  );
};

export default Navbar;
