// Import libraries
import PropTypes from "prop-types";

// Import material ui components
import Button from "@mui/material/Button";
import AddIcon from "@mui/icons-material/Add";

const FileInput = ({ onChange, accept }) => {
  return (
    <Button size="small" variant="contained" component="label" color="primary">
      {" "}
      <AddIcon /> Upload a file
      <input accept={accept} type="file" hidden onChange={onChange} />
    </Button>
  );
};

FileInput.defaultProps = {
  accept: "",
};

FileInput.propTypes = {
  onChange: PropTypes.func.isRequired,
  accept: PropTypes.string,
};

export default FileInput;
