

// Import library
import { Outlet } from 'react-router-dom';

// Import Components
import Navbar from '../UI/Navbar';

// Import material ui component
import Box from '@mui/material/Box'

const SharedLayout = () => {
  return (
    <>
        <Navbar />
        <Box padding="2em">
          <Outlet />
        </Box>
    </>
  )
}

export default SharedLayout