// Import libraries
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

// Import components
import DeleteHouse from "./DeleteHouse";

// Import material ui components
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";

const HouseCard = ({ id, name, location, image }) => {
  return (
    <Card sx={{ maxWidth: 345 }}>
      <CardMedia component="img" image={image} alt={name} />
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
          {name}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          {location}
        </Typography>
      </CardContent>
      <CardActions>
        <Box display="flex" gap="1em">
          <Link
            to={`/houses/${id}`}
            style={{ textDecoration: "none", color: "inherit" }}
          >
            <Button variant="contained" size="small">
              Edit
            </Button>
          </Link>
          <DeleteHouse id={id} />
        </Box>
      </CardActions>
    </Card>
  );
};

HouseCard.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  location: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
};

export default HouseCard;
