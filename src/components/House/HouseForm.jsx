// Import libraries
import PropTypes from "prop-types";
import { useFormik } from "formik";

// Import material ui component
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";

// Import component
import FileInput from "../../components/UI/FileInput";

// Import validation
import { IMAGE_TYPE } from "../../constants/file";
import { useState } from "react";

const HouseForm = ({
  name,
  location,
  image,
  onSubmitHouse,
  validationSchema,
}) => {
  const [imageUrl, setImageUrl] = useState("");

  function onSubmit(values) {
    onSubmitHouse(values);
  }

  const formik = useFormik({
    initialValues: {
      name,
      location,
      image,
      // This is meant for the file to be uploaded to the server
      file: undefined,
    },
    validationSchema,
    onSubmit,
  });

  function changeImageHandler(e) {
    if (e.target.files.length > 0) {
      let fileUpload = e.target.files[0];

      formik.setFieldValue("file", fileUpload);

      if (imageUrl) {
        URL.revokeObjectURL(imageUrl);
      }

      setImageUrl(URL.createObjectURL(fileUpload));
    }
  }

  return (
    <form onSubmit={formik.handleSubmit}>
      <Box
        display={"flex"}
        flexDirection={"column"}
        sx={{ maxWidth: "345px" }}
        marginX="auto"
        direction="column"
        gap="2em"
      >
        <Box
          display={"flex"}
          flexDirection={"column"}
          marginX="auto"
          direction="column"
          gap="0.75em"
          width="100%"
        >
          <Box>
            <TextField
              error={formik.errors.name}
              helperText={formik.errors.name}
              id="house-name-input"
              name="name"
              label="Name"
              type="text"
              onChange={formik.handleChange}
              fullWidth
              value={formik.values.name}
            />
          </Box>
          <Box>
            <TextField
              error={formik.errors.location}
              helperText={formik.errors.location}
              id="house-location-input"
              name="location"
              label="Location"
              type="text"
              fullWidth
              onChange={formik.handleChange}
              value={formik.values.location}
            />
          </Box>
          <Box
            component="img"
            sx={{
              width: "100%",
              marginX: "auto",
            }}
            alt={name}
            src={imageUrl || formik.values.image}
          />
          <Box sx={{ textAlign: "center" }}>
            <FileInput
              accept={IMAGE_TYPE.join(",")}
              onChange={changeImageHandler}
            />

            <Box marginTop={'0.5em'}>
              <Typography variant="caption" color="error">
                {formik.errors.file}
              </Typography>
            </Box>
          </Box>
        </Box>
        <Box sx={{ textAlign: "center" }}>
          <Button type="submit" size="large" color="primary">
            Submit
          </Button>
        </Box>
      </Box>
    </form>
  );
};

HouseForm.defaultProps = {
  name: "",
  location: "",
  image: "",
};

HouseForm.propTypes = {
  name: PropTypes.string,
  location: PropTypes.string,
  image: PropTypes.string,
  onSubmitHouse: PropTypes.func.isRequired,
  validationSchema: PropTypes.object.isRequired,
};

export default HouseForm;
