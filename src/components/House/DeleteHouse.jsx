// Import libraries
import React, { useState } from "react";
import { useDispatch } from "react-redux";

// Import material ui component
import Button from "@mui/material/Button";

// Import components
import ConfirmDialog from "../UI/Confirmation";
import Typography from "@mui/material/Typography";

// Import house functionality
import { deleteHouse } from "../../features/house/house";

const DeleteHouse = ({ id }) => {
  const [open, setOpen] = useState(false);

  const dispatch = useDispatch();

  function onClose() {
    setOpen(false);
  }

  function onOpen() {
    setOpen(true);
  }

  function onConfirm() {
    dispatch(deleteHouse({ id }));
  }

  return (
    <>
      <ConfirmDialog
        open={open}
        onConfirm={onConfirm}
        setOpen={onClose}
        title="Delete House"
      >
        <Typography variant="p">
          Are you sure you want to delete this house?
        </Typography>
      </ConfirmDialog>
      <Button onClick={onOpen} color="error" variant="contained" size="small">
        Delete
      </Button>
    </>
  );
};

export default DeleteHouse;
