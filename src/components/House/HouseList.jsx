// Import libraries
import { useEffect } from 'react';
import { useDispatch,useSelector } from 'react-redux';
import { fetchHouses, selectAllHouses } from '../../features/house/house';

// Import components
import HouseCard from './HouseCard';

// Import material ui components
import Box from '@mui/material/Box'

const HouseList = () => {
  const houses = useSelector(selectAllHouses);
  
  const dispatch = useDispatch();

  useEffect(()=>{
    dispatch(fetchHouses());
  },[dispatch])

  return (
    <Box 
      display={"flex"} 
      flexWrap="wrap" 
      flexDirection="row" 
      gap="2em"
      justifyContent="start"
    >
      {
        houses.map((house)=>(
          <Box
            key={house.id}
          >
            <HouseCard
              id={house.id}
              location={house.location}
              image={house.image}
              name={house.name}
            />
          </Box>
        ))
      }
    </Box>
  )
}

export default HouseList