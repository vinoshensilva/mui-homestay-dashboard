// Import libraries
import { useDispatch } from "react-redux";

// Import house functionality
import { createHouse } from "../../features/house/house";
import { jsonToFormData } from "../../functions/formdata";

// Import validation schema
import { createHouseValidationSchema } from "../../validation/house/HouseValidation";

// Import components
import HouseForm from "./HouseForm";

const AddHouse = () => {
  const dispatch = useDispatch();

  const submitHandler = (values) => {
    const formData = jsonToFormData(values);

    dispatch(createHouse({ body: formData }));
  };

  return (
    <>
      {
        <HouseForm
          validationSchema={createHouseValidationSchema}
          onSubmitHouse={submitHandler}
        />
      }
    </>
  );
};

export default AddHouse;
