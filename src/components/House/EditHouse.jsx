// Import libraries
import PropTypes from "prop-types";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";

// Import house functionality
import { selectHouseById, updateHouse } from "../../features/house/house";
import { jsonToFormData } from "../../functions/formdata";

// Import components
import HouseForm from "./HouseForm";

// Import validation schema
import { updateHouseValidationSchema } from "../../validation/house/HouseValidation";

const EditHouse = ({ id }) => {
  const house = useSelector((state) => selectHouseById(state, id));

  const dispatch = useDispatch();

  const submitHandler = (values) => {
    const formData = jsonToFormData(values);

    dispatch(updateHouse({ id, body: formData }));
  };

  return (
    <>
      {house && (
        <HouseForm
          image={house.image}
          location={house.location}
          name={house.name}
          validationSchema={updateHouseValidationSchema}
          onSubmitHouse={submitHandler}
        />
      )}
    </>
  );
};

EditHouse.propTypes = {
  id: PropTypes.string.isRequired,
};

export default EditHouse;
