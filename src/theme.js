import { createTheme } from "@mui/material";
import { indigo } from '@mui/material/colors';

const theme = createTheme({
    palette: {
      primary: {
        main: indigo[500]
      },
      success: {
        main: '#4caf50'
      }
    },
  })

  export default theme;