// Import libraries
import {
    createSlice,
    createAsyncThunk,
} from '@reduxjs/toolkit';

// Import functions
import { client } from '../../api/client';

// Import classes
import HouseSerializer from '../../class/HouseSerializer';

// Import constants
import { 
  FAILED, 
  LOADING, 
  SUCCEEDED 
} from '../../constants/thunk';

const initialState = {
    status: 'idle',
    error: null,
    houses: []
};

export const fetchHouses = createAsyncThunk('houses',async()=>{
    const response = await client.get('/api/houses');

    return response.data.houses.map(HouseSerializer.Deserialize);
});

export const getHouse = createAsyncThunk('houses/getById',async({id: houseId})=>{
  const response = await client.get(`/api/houses/${houseId}`);

  return HouseSerializer.Deserialize(response.data.house)
})

export const updateHouse = createAsyncThunk('houses/updateById',async({id: houseId,body: formData})=>{
  const response = await client.patchFormData(`/api/houses/${houseId}`,formData);

  return HouseSerializer.Deserialize(response.data.house)
})

export const deleteHouse = createAsyncThunk('houses/deleteById',async({id: houseId})=>{
  await client.delete(`/api/houses/${houseId}`);

  return {id: houseId};
})

export const createHouse = createAsyncThunk('houses/createHouse',async({body: formData})=>{

  const response = await client.postFormData(`/api/houses`,formData);

  return HouseSerializer.Deserialize(response.data.house);
});

const housesSlice = createSlice({
    name: 'houses',
    initialState,
    extraReducers(builder) {
      // Get
      builder
      .addCase(getHouse.pending,(state,action)=>{
        state.status = 'loading';
      })
      .addCase(getHouse.fulfilled,(state,action)=>{
          state.status = SUCCEEDED;

          const {
            id,
            name,
            location,
            image
          } = action.payload;

          const existingHouse = state.houses.find(house => house.id === id)

          if(existingHouse){
            existingHouse.name = name;
            existingHouse.location = location;
            existingHouse.image = image;
          }
          else {
            state.houses.push(action.payload);
          }
      })
      .addCase(getHouse.rejected,(state,action)=>{
        state.status = FAILED;
        state.error = action.error.message;
      })
      // Delete
      builder
      .addCase(deleteHouse.pending,(state,action)=>{
        state.status = 'loading';
      })
      .addCase(deleteHouse.fulfilled,(state,action)=>{
          state.status = SUCCEEDED;

          const {
            id,
          } = action.payload;

          const houseIndex = state.houses.findIndex(house => house.id === id);
          
          if(houseIndex > -1){
            state.houses.splice(houseIndex,1);
          }
      })
      .addCase(deleteHouse.rejected,(state,action)=>{
        state.status = FAILED;
        state.error = action.error.message;
      })
      // Update
      builder
      .addCase(updateHouse.pending,(state,action)=>{
        state.status = 'loading';
      })
      .addCase(updateHouse.fulfilled,(state,action)=>{
          state.status = SUCCEEDED;

          const {
            id,
            name,
            location,
            image
          } = action.payload;

          const existingHouse = state.houses.find(house => house.id === id)

          if(existingHouse){
            existingHouse.name = name;
            existingHouse.location = location;
            existingHouse.image = image;
          }
          else {
            state.houses.push(action.payload);
          }
      })
      .addCase(updateHouse.rejected,(state,action)=>{
        state.status = FAILED;
        state.error = action.error.message;
      })
      // Create
      builder
      .addCase(createHouse.pending,(state,action)=>{
        state.status = 'loading';
      })
      .addCase(createHouse.fulfilled,(state,action)=>{
          state.status = SUCCEEDED;

          state.houses.push(action.payload);
      })
      .addCase(createHouse.rejected,(state,action)=>{
        state.status = FAILED;
        state.error = action.error.message;
      })
      // Fetch
      builder
      .addCase(fetchHouses.pending,(state,action)=>{
        state.status = LOADING;
      })
      .addCase(fetchHouses.fulfilled,(state,action)=>{
        state.status = SUCCEEDED;

        state.houses = action.payload;
      })
      .addCase(fetchHouses.rejected,(state,action)=>{
        state.status = FAILED;
        state.error = action.error.message;
      })
    }
});

export const selectAllHouses = state => state.houses.houses;

export const selectHouseById = (state,houseId) => state.houses.houses.find(house => house.id === houseId)

export default housesSlice.reducer;