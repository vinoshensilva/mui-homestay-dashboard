// A tiny wrapper around fetch(), borrowed from
// https://kentcdodds.com/blog/replace-axios-with-a-simple-custom-fetch-wrapper

export async function client(endpoint, { body, ...customConfig } = {}) {
  const headers = { 'Content-Type': 'application/json' }

  const config = {
    method: body ? 'POST' : 'GET',
    ...customConfig,
    headers: {
      ...headers,
      ...customConfig.headers,
    },
  }

  if (body instanceof FormData) {
    config.body = body;
  }
  else if(body){
    config.body = JSON.stringify(body);
  }

  let data
  try {
    const response = await window.fetch(endpoint, config)

    let data = undefined;

    const contentType = response.headers.get("content-type"); 

    if(contentType && contentType.indexOf("application/json") !== -1){
      data = await response.json()
    }
    
    if (response.ok) {
      // Return a result object similar to Axios
      return {
        status: response.status,
        data,
        headers: response.headers,
        url: response.url,
      }
    }
    throw new Error(response.statusText)
  } catch (err) {
    return Promise.reject(err.message ? err.message : data)
  }
}

client.get = function (endpoint, customConfig = {}) {
  return client(endpoint, { ...customConfig, method: 'GET' })
}

client.post = function (endpoint, body, customConfig = {}) {
  return client(endpoint, { ...customConfig, body })
}

client.postFormData = function (endpoint, body, customConfig = {}) {
  return client(endpoint, { ...customConfig, body, headers: { "Content-Type": "multipart/form-data" }})
}

client.patch = function (endpoint, body, customConfig = {}) {
  return client(endpoint, { ...customConfig, body,method: 'PATCH' })
}

client.patchFormData = function (endpoint, body, customConfig = {}) {
  return client(endpoint, { ...customConfig, body,method: 'PATCH',headers: { "Content-Type": "multipart/form-data" }})
}

client.delete = function (endpoint, body, customConfig = {}) {
  return client(endpoint, { ...customConfig,method: 'DELETE' })
}