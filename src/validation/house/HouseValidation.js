// Import libraries
import { 
    string,
    object,
    mixed 
} from 'yup';

// Import constants
import { IMAGE_SIZE,IMAGE_TYPE } from '../../constants/file';

export const createHouseValidationSchema = object().shape({
    name: string().min(1).required('House name is required'),
    location: string().min(1).required('House location is required'),
    file: mixed()
    .nullable()
    .required('House image is required')
    .test("IMAGE_SIZE", "Uploaded file is too big.", 
        value => !value || (value && value.size <= IMAGE_SIZE))
    .test("IMAGE_FORMAT", "Uploaded file has unsupported format.", 
        value => !value || (value && IMAGE_TYPE.includes(value.type)))
});

export const updateHouseValidationSchema = object().shape({
    name: string().min(1),
    location: string().min(1),
    file: mixed()
    .nullable()
    .notRequired()
    .test("IMAGE_SIZE", "Uploaded file is too big.", 
        value => !value || (value && value.size <= IMAGE_SIZE))
    .test("IMAGE_FORMAT", "Uploaded file has unsupported format.", 
        value => !value || (value && IMAGE_TYPE.includes(value.type)))
});