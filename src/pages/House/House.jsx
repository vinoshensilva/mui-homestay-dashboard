// Import libraries
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";

// Import material uicomponents
import Box from "@mui/material/Box";

// Import house functionality
import { getHouse } from "../../features/house/house";

// Import components
import Backdrop from "../../components/UI/Backdrop";
import EditHouse from "../../components/House/EditHouse";

// Import constants
import { LOADING } from "../../constants/thunk";

const House = () => {
  const { houseId } = useParams();
  const dispatch = useDispatch();

  const status = useSelector((state) => state.houses.status);

  useEffect(() => {
    dispatch(getHouse({ id: houseId }));
  }, [dispatch, houseId]);

  return (
    <Box>
      {status === LOADING && <Backdrop />}
      <EditHouse id={houseId} />
    </Box>
  );
};

export default House;
