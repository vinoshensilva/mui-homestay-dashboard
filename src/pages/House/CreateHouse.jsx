// Import libraries
import { useSelector } from "react-redux";

// Import components
import AddHouse from "../../components/House/AddHouse";
import LoadingBackdrop from "../../components/UI/Backdrop";

// Import material ui
import Box from "@mui/material/Box";

// Import constants
import { LOADING } from "../../constants/thunk";

const CreateHouse = () => {
  const status = useSelector((state) => state.houses.status);

  return (
    <Box>
      {status === LOADING && <LoadingBackdrop />}
      <AddHouse />
    </Box>
  );
};

export default CreateHouse;
