// import material ui components
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";

const NotFound = () => {
  return (
    <Box
      display="flex"
      justifyContent="center"
      alignItems="center"
      textAlign="center"
      height="80vh"
    >
      <Box>
        <Typography fontWeight="300" variant="h2" color="text.primary">
          404 Not Found
        </Typography>
      </Box>
    </Box>
  );
};

export default NotFound;
