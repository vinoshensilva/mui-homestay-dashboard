// Import libraries
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

// Import components
import HouseList from "../components/House/HouseList";
import LoadingBackdrop from "../components/UI/Backdrop";
import Button from "@mui/material/Button";
import Box from "@mui/material/Box";

// Import constants
import { LOADING } from "../constants/thunk";

const Home = () => {
  const status = useSelector((state) => state.houses.status);

  return (
    <>
      {status === LOADING && <LoadingBackdrop />}
      <Box display="flex" gap="2em" flexDirection="column">
        <Box fullWidth sx={{ textAlign: "center" }}>
          <Link
            to={"/houses/create"}
            style={{ textDecoration: "none", color: "inherit" }}
          >
            <Button variant="contained">Create House</Button>
          </Link>
        </Box>
        <HouseList />
      </Box>
    </>
  );
};

export default Home;
