// Import libraries
import { 
  BrowserRouter,
  Routes,
  Route,
  Navigate
} from 'react-router-dom';

import { ThemeProvider } from '@mui/material';

// Import constants
import { DEVELOPMENT } from './constants/environments';

// Import mock server
import { startServer } from './mirage';

// Import page
import Home from './pages/Home';
import House from './pages/House/House';
import CreateHouse from './pages/House/CreateHouse';
import NotFound from './pages/NotFound';

// Import components
import SharedLayout from './components/Layout/SharedLayout';

// Import theme
import theme from './theme';

if(process.env.NODE_ENV === DEVELOPMENT){
  startServer();
}

function App() {
  return (
    <ThemeProvider theme={theme}>
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<SharedLayout />}  >
            <Route index element={<Home />} />
            <Route path='houses'>
              <Route path='create' element={<CreateHouse />} />
                <Route path=":houseId" element={<House />} />
            </Route>
            <Route 
              path='404'
              element={<NotFound />}
            />
          </Route>
          <Route 
            path='*'
            element={<Navigate to={'404'} replace />}
          />
        </Routes>
      </BrowserRouter>
    </ThemeProvider>
  );
}

export default App;