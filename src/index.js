
import { createRoot } from 'react-dom/client';
import { Provider } from 'react-redux';
import React from 'react';

// Import store
import { store } from './app/store';

import App from './App';

// Import main style
import './index.css';

const container = document.getElementById('root');
const root = createRoot(container);

root.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>
);
