import { configureStore } from '@reduxjs/toolkit';
import housesReducer from '../features/house/house';

export const store = configureStore({
  reducer: {
    houses: housesReducer
  },
});
